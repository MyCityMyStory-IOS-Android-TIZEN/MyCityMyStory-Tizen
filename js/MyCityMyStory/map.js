function myMap() {
	
    var mapCanvas = document.getElementById("mapA");
    var mapOptions = {
        center: new google.maps.LatLng(36.848913,10.174311),
        zoom: 10
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var infoWindow = new google.maps.InfoWindow({map: map});
    infoWindow.setPosition(new google.maps.LatLng( 36.848913,10.174311));
     map.setCenter(new google.maps.LatLng( 36.848913,10.174311));
    
}