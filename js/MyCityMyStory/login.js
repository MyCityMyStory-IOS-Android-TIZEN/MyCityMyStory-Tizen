
function signin(){   
	
	var email = document.getElementById('login').value;
	var password = document.getElementById('login-psw').value;
	if (email == '' || password =='') {
		Materialize.toast('Please fill all fields...');
	}
	else{
	firebase.auth().signInWithEmailAndPassword(email, password).then(function(user){
		firebase.database().ref('Users/' + user.uid).once('value').then(function(snapshot) {
            username = snapshot.val().userName;
            urlPhoto = snapshot.val().urlImage;
            var ObjetUSER= '{ "id":"'+user.uid+'", "username":"'+username+'", "urlPhoto":"'+urlPhoto+'", "email":"'+email+'", "password":"'+password+'"}';
            sessionStorage.setItem("CURRENT_USER",ObjetUSER);
            var CnxType= '{ "facebook":"No","google":"No","firebase":"Yes"}';
    		sessionStorage.setItem("LOGINType",CnxType); 
    		
    		
     	    window.location.href='locationUserCity.html';
          
		});
 
	}).catch(function(error) {
		  // Handle Errors here.
		  var errorCode = error.code;
		  var errorMessage = error.message;
		  
		  Materialize.toast(errorMessage);
		  // ...
		});
	}
}

function sendPasswordReset(email) {
	var email = document.getElementById('email').value;

	if (email == '') {
		Materialize.toast('Please fill all fields...');
		
	} else{
		 // [START sendpasswordemail]
	    firebase.auth().sendPasswordResetEmail(email).then(function() {
	      Materialize.toast('Password Reset Email Sent!');
	      window.location.href="login.html";
	      // [END_EXCLUDE]
	    }).catch(function(error) {
	      // Handle Errors here.
	      var errorCode = error.code;
	      var errorMessage = error.message;
	      // [START_EXCLUDE]
	      if (errorCode == 'auth/invalid-email') {
 	        Materialize.toast(errorMessage);
	      } else if (errorCode == 'auth/user-not-found') {
	    	  Materialize.toast(errorMessage);
	      }
	      console.log(error);
	      // [END_EXCLUDE]
	    });
	    // [END sendpasswordemail];
	}

   
  }


function logout(){
	
	//
	
	var obj = JSON.parse(sessionStorage.getItem("LOGINType"));
	if (obj.facebook === "Yes"){
		
		LogoutFB();
	}else if (obj.firebase === "Yes"){
		firebase.auth().signOut().then(function() {
			  window.location.href="login.html";
			}, function(error) {
			  // An error happened.
			});
	}else{
		console.log("IDK What happen");
	}
	 localStorage.removeItem('CURRENT_USER');
	 localStorage.removeItem('CountryUser');
	 localStorage.removeItem('LOGINType');
	 localStorage.removeItem('cityObjectUser');
     window.location.href="login.html";

	
}

/***************FACEBOOK***********************/
function signUpByFaceBook(email,password,urlImage,userName){

	firebase.auth().createUserWithEmailAndPassword(email, password).then(function(user) {
		var ref = firebase.database().ref();
	    var usersRef = ref.child('Users/'+user.uid);
        var ObjetUSER= '{ "id":"'+user.uid+'", "username":"'+userName+'", "urlPhoto":"'+urlImage+'", "email":"'+email+'", "password":"'+password+'"}';
		sessionStorage.setItem("CURRENT_USER",ObjetUSER); 
		var CnxType= '{ "facebook":"Yes","google":"No","firebase":"No"}';
		sessionStorage.setItem("LOGINType",CnxType); 
		writeUserData(userName,email,password,urlImage,user.uid,usersRef);
	}).catch(function(error) {
		  // Handle Errors here.
		  var errorCode = error.code;
		  var errorMessage = error.message;
		  // ...
		  console.log(error.message);
		  //auth/email-already-in-use
		 if (errorCode === "auth/email-already-in-use"){
			  firebase.auth().signInWithEmailAndPassword(email, password).then(function(user){
					var ref = firebase.database().ref();
				    var usersRef = ref.child('Users/'+user.uid);
			        var ObjetUSER= '{ "id":"'+user.uid+'", "username":"'+userName+'", "urlPhoto":"'+urlImage+'", "email":"'+email+'", "password":"'+password+'"}';
					sessionStorage.setItem("CURRENT_USER",ObjetUSER);
					var CnxType= '{ "facebook":"Yes","google":"No","firebase":"No"}';
					sessionStorage.setItem("LOGINType",CnxType); 
					
					getDateUSER();
					getDataUSERCity();

			  }).catch(function(error) {
				  // Handle Errors here.
				  var errorCode = error.code;
				  var errorMessage = error.message;
				  
				  console.log(errorMessage);
				  // ...
				});
		  }
		});
	
}

function FBLogin(){
    console.log("inside login");
    window.authWin = window.open(final_uri, "blank", "", true);
    var CnxType= '{ "facebook":"Yes","google":"No","firebase":"No"}';
	sessionStorage.setItem("LOGINType",CnxType); 
    montiorURL();
}

function montiorURL() {
	console.log("montiorURL");
    window.int = self.setInterval(function () {
        window.authCount = window.authCount + 1;

        if (window.authWin && window.authWin.location) { 
            var currentURL = window.authWin.location.href;
            var inCallback = currentURL.indexOf("?code");
            if (inCallback >= 0) {
                var codeData = currentURL.substr(currentURL.indexOf("="));
                var code=codeData.substring(1);
                getAccesstoken(code);
            }
        }
        if (window.authCount > 30) {
            alert('30 seconds time out');
            window.authCount  =0;
            window.clearInterval(int)
            window.authWin.close();
        }
	}, 500);
}

function  getAccesstoken(code){
    $.ajax({
        type : "GET",
        url :'https://graph.facebook.com/oauth/access_token?client_id=381499552212060&redirect_uri=https://www.facebook.com/connect/login_success.html&client_secret=2a940c2a243f5907af1f11e0ce11cc5a&code='+code,
        success : function(data) {
            try {
            	console.log("getAccesstoken acess success");
                accesstoken=data;
                access_token=parseToken(accesstoken);
                localStorage['accesstoken']=access_token;
                window.clearInterval(int)
                window.authWin.close();
                window.location.href="locationUserCity.html"
			 
            }
            catch (e) {
                console.log(e);
            }
        },
        error : function() {
          //  $.mobile.changePage("#errpage");
            console.log("acess token error");
        }
    });    
}

function parseToken(accesstoken){
    var c = accesstoken.indexOf('access_token') ; 
    var L = accesstoken.indexOf('&expires') ;
    var y = accesstoken.substring(0, c) + accesstoken.substring(L, accesstoken.length);
    var remaining = accesstoken.replace(y,'');
    return (remaining.substring(13));
}
function getProfile() {
	$.ajax({
        type : "GET",
        dataType : 'json',
        url : 'https://graph.facebook.com/me?fields=email,first_name,last_name,education,location,languages,hometown,picture&permissions=read_stream&access_token=' +localStorage['accesstoken'] ,
        success : function(data1) {
	        console.log("getProfile() data = " + data1);
            var firstname=data1.first_name;
            var lastname=data1.last_name;  
            var email=data1.email;   
            var pic_url=data1.picture.data.url;
            console.log(firstname);
            console.log(lastname);
            console.log(email);
            console.log(pic_url);
            signUpByFaceBook(email,firstname+"_"+lastname,pic_url,firstname+" "+lastname);
            },
        error : function() {
        	$.mobile.changePage("#errpage");
            console.log("Unable to get your friends on Facebook");
        }
    });
}

 
function LogoutFB()
{
    $.ajax({
        type : "GET",
        url :'https://www.facebook.com/logout.php?next=https://www.facebook.com/connect/login_success.html&access_token='+localStorage['accesstoken'],
        success : function(data) {
			//Disconnect from Firebase & Fisebook
			firebase.auth().signOut().then(function() {
	            //window.location.href="index.html"        
			}, function(error) {
			  // An error happened.
			});
	        
        },
    	error: function(){console.log("error");}
    });
}
function writeUserData(userName, email ,password, imageUrl,id,usersRef) {
	  
	
	var userRef = usersRef.set({
	 email: email,
	 userName: userName,
	 password: password,
	 urlImage: imageUrl
	}).then(function(user){
	    //window.location.href="index-sliced.html";
		console.log("yes DataUser COnnected saved ");
		getDateUSER();
		getDataUSERCity();
	});
	}
/**********************************************/



function getDateUSER(){
	 

	var obj = JSON.parse(sessionStorage.getItem("CURRENT_USER"));
		console.log(obj.username);
		document.getElementById("userNameToolbarId").textContent = obj.username;
//			document.getElementById("bannerId").textContent = obj.username;
//   		document.getElementById("emailId").textContent = obj.email;
 		 document.getElementById("imageUserID").src =obj.urlPhoto;
 		 document.getElementById("userNameIDDrawerBar").textContent =obj.username;
//   		 document.getElementById("photoProfileID").src =obj.urlPhoto;
 		
 	}
function getDataUSERCity(){
	 

	var obj = JSON.parse(sessionStorage.getItem("cityObjectUser"));
	
	 document.getElementById("cityUserID").textContent = obj.city;
	 
	// document.getElementById("CityOfUserID").textContent = obj.city;
	 
		//document.getElementById("bannerId").textContent = obj.username;
		//document.getElementById("emailId").textContent = obj.email;
		//document.getElementById("toolBarUserNameID").textContent = obj.username;
		//document.getElementById("imageUserID").src =obj.urlPhoto;
 		//document.getElementById("BannerTitleID").textContent = obj.username;
 		//document.getElementById("userEmailID").textContent = obj.email;
}