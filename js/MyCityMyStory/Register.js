
function signup(files){

	 
	function uploadimage(userName,email,password,id){
		var ref = firebase.database().ref();

		var usersRef = ref.child('Users/'+id);
		 // Create a root reference
	    var storageRef = firebase.storage().ref();
	    
 	    var uploadTask = storageRef.child('images/'+id+'.jpg').put(files[0]);

	 // Register three observers:
	 // 1. 'state_changed' observer, called any time the state changes
	 // 2. Error observer, called on failure
	 // 3. Completion observer, called on successful completion
	 uploadTask.on('state_changed', function(snapshot){
	   // Observe state change events such as progress, pause, and resume
	   // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
	   var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
	   console.log('Upload is ' + progress + '% done');
	   switch (snapshot.state) {
	     case firebase.storage.TaskState.PAUSED: // or 'paused'
	       console.log('Upload is paused');
	       break;
	     case firebase.storage.TaskState.RUNNING: // or 'running'
	       console.log('Upload is running');
	       break;
	   }
	 }, function(error) {
		 Materialize.toast(error);
	   // Handle unsuccessful uploads
	 }, function() {
	   // Handle successful uploads on complete
	   // For instance, get the download URL: https://firebasestorage.googleapis.com/...
	   var downloadURL = uploadTask.snapshot.downloadURL;
	   
	   writeUserData(userName,email,password,downloadURL,id,usersRef);
	 });
	}
	  




	
	var email = document.getElementById('login').value;
	var password = document.getElementById('login-psw').value;
	var userName = document.getElementById('userName').value;
	if (email == '' || password =='' || userName =='') {
		Materialize.toast('Please fill all fields...');
	}else{
		firebase.auth().createUserWithEmailAndPassword(email, password).then(function(user) {
		uploadimage(userName,email,password,user.uid);
	   }).catch(function(error) {
		  // Handle Errors here.
		  var errorCode = error.code;
		  var errorMessage = error.message;
		  // ...
		  Materialize.toast(errorMessage);
		});
    }
	
}

function writeUserData(userName, email ,password, imageUrl,id,usersRef) {

	var userRef = usersRef.set({
	 email: email,
	 userName: userName,
	 password: password,
	 urlImage: imageUrl,
	 id: id
	}).then(function(user){
		
		
		window.location.href="login.html"
		  
	});
}
