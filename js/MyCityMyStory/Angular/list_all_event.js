var app = angular.module("list_all_event_angular", [ 'ui.router' ]);
app.controller("allEventController", function($scope, $http) {


 

	$scope.loadAllEvents = function () {
		 var allEvent ;
		var obj = JSON.parse(sessionStorage.getItem("cityObjectUser"));
         // use $.param jQuery function to serialize data from JSON 
         var config = {
             headers : {
                 'Ocp-Apim-Subscription-Key': '72848bd8222a46e8a7de8626e7c3f9fd'
             }
         }
         $http.post('https://api.allevents.in/events/list/?city='+obj.city+'&state='+obj.region_code+'&country='+obj.country, {}, config)
         .success(function (res, status, headers, config) {
        	 console.log(res.data);
             $scope.listAllEvent = res.data;   
             allEvent =  $scope.listAllEvent  ; 
         })
         .error(function (data, status, header, config) {
   		      Materialize.toast("Plz Check your netwrok");
             // $scope.ResponseDetails = "Data: " + data +
              //   "<hr />status: " + status +
               //  "<hr />headers: " + header +
              //   "<hr />config: " + config;
         });
     };
     
     $scope.loadAllEventsByCat = function () {
    	 var allEvent ;
 		var obj = JSON.parse(sessionStorage.getItem("cityObjectUser"));
 		var objCat = sessionStorage.getItem("events_category");
          // use $.param jQuery function to serialize data from JSON 
          var config = {
              headers : {
                  'Ocp-Apim-Subscription-Key': '72848bd8222a46e8a7de8626e7c3f9fd'
              }
          }
          $http.post('https://api.allevents.in/events/list/?city='+obj.city+'&state='+obj.region_code+'&country='+obj.country+'&category='+objCat, {}, config)
          .success(function (res, status, headers, config) {
         	 console.log(res.data);
              $scope.listAllEvent = res.data;   
              allEvent =  $scope.listAllEvent  ; 
          })
          .error(function (data, status, header, config) {
    		      Materialize.toast("Plz Check your netwrok");
              // $scope.ResponseDetails = "Data: " + data +
               //   "<hr />status: " + status +
                //  "<hr />headers: " + header +
               //   "<hr />config: " + config;
          });
      };
     $scope.findValue = function(enteredValue) {     
         angular.forEach(allEvent, function(value, key) {
             if (key === enteredValue) {
            	  $scope.listAllEvent = res.data;   
             }
         });
     };
     
    $scope.GoToDetail = function(f) {
  		sessionStorage.setItem('event', JSON.stringify(f));
 		window.location.href = "detailEvent.html";
  	}
    
    $scope.GoT = function(f) {

  	}


});