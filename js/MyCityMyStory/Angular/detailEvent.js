var app = angular.module("descriptin_event_angular", [ 'ui.router' ]);
app.controller("descriptionEventController", function($scope, $http) {

	$scope.loadDescriptionEvent = function() {
		var obj = JSON.parse(sessionStorage.getItem("event"));

		$http.get("http://elchebbi-ahmed.alwaysdata.net/mycitymystory/getDesriptionEvent.php?idEventFb="+obj.event_id).success(function(data) {
			$scope.description = data;

		}).error(function(error) {
			Materialize.toast("NETWORK PROBLEME...");
			 
		});

	};
	
	$scope.addToMyCalendar  = function(){
		var obj = JSON.parse(sessionStorage.getItem("event"));
		var objUser = JSON.parse(sessionStorage.getItem("CURRENT_USER"));

		var eventString = sessionStorage.getItem("event");
		console.log(eventString);
		var ref = firebase.database().ref();
		var usersRef = ref.child('Users/'+objUser.id+'/MyCalendar');
	    var s = usersRef.child(obj.event_id);
		var userRef = s.set(eventString).then(function(x){
				Materialize.toast("Event Added to your calendar with succes");
			}).catch(function(err){
	    Materialize.toast("This Event can not be added ");
			});
	};

	
	$scope.sendInvit = function(){
		var obj = JSON.parse(sessionStorage.getItem("event"));
		var objUser = JSON.parse(sessionStorage.getItem("CURRENT_USER"));
		
		var eventString = sessionStorage.getItem("event");
		var ref = firebase.database().ref();
		
		firebase.database().ref('Users').once('value').then(function(snapshot) {
			 snapshot.forEach(function(childSnapshot) {
				 	 var childData = childSnapshot.val();
				if (objUser.id != childData.id ) {
				 var usersRef = ref.child('Users/'+childData.id+'/Notification/'+objUser.id+'_'+obj.event_id);
				 	usersRef.set({ from_user_name : objUser.username , from : objUser.id , event : eventString});
				};	
			 });
		});
	}

    $scope.paticipate = function () {
    	var objEvent = JSON.parse(sessionStorage.getItem("event"));
		var objUser = JSON.parse(sessionStorage.getItem("CURRENT_USER"));
		
	         var ref = firebase.database().ref();
        var usersRef = ref.child('partipations/'+objUser.id+'_'+objEvent.event_id);
 
       
           var userRef = usersRef.set({
					"userId" : objUser.id,
					"eventId" : objEvent.event_id
               }).then(function(x){
           	 				Materialize.toast("Thank you for participation ^^ :D");
           	 			}).catch(function(err){
           	              Materialize.toast("NetWork Problem");
           	 			});                        
    	}
	 $scope.getNot = function(){
		 $scope.from_user_name="c";
 
		 $scope.listNotification=null;
		 console.log("test");
		var objUser = JSON.parse(sessionStorage.getItem("CURRENT_USER"));

		var listNotification = document.getElementById("listNotification");
		
		
		 var ref = firebase.database().ref("Users/"+objUser.id+"/Notification");
		ref.on("value", function(snapshot) {
				 $scope.listNotification=null;
				 listNotification.innerHTML =""
				snapshot.forEach(function(childSnapshot) {
					var changedPost = childSnapshot.val();
					var eventObj =  JSON.parse(changedPost.event);
  					listNotification.innerHTML +="<div class='notification notification-success'><a class='close-notification no-smoothState'><i class='ion-android-close'></i></a><p>"+changedPost.from_user_name+"<h3> invite you </h3> to participate"+eventObj.eventname+"</p><span>7 - 10 pm</span>"
				});
				 
				});
 
	 }
});
 